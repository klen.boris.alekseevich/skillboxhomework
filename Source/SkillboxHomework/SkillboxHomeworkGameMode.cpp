// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkillboxHomeworkGameMode.h"
#include "SkillboxHomeworkHUD.h"
#include "SkillboxHomeworkCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASkillboxHomeworkGameMode::ASkillboxHomeworkGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASkillboxHomeworkHUD::StaticClass();
}
