// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SkillboxHomeworkGameMode.generated.h"

UCLASS(minimalapi)
class ASkillboxHomeworkGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASkillboxHomeworkGameMode();
};



