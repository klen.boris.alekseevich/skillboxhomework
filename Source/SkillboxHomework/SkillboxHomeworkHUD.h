// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SkillboxHomeworkHUD.generated.h"

UCLASS()
class ASkillboxHomeworkHUD : public AHUD
{
	GENERATED_BODY()

public:
	ASkillboxHomeworkHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

